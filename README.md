# PBbars - README #
---

### Overview ###

The **PBbars** tool can be used to display a simple and customizable bar graphic on a website without any additional libraries. Everything is generated with PHP, HTML and CSS code. The whole example is only about 54 KB in size. If you minimize the files the size is reduzed to 16 KB.

### Screenshot ###

![PBbars Example Graph](development/readme/pbbars.png "PBbars Example Graph")

### Setup ###

* Copy the whole directory **pbbars** to your webhost.
* Edit the configuration file **pbbars/config/config.php**.
* If necessary edit the CSS file **pbbars/css/pbbars.css**.
* Open the example file **pbbars/index.php** to see the result.
* Use your own file and include the function file **pbbars/include/function.php**.
* Then pass the graph data to the function like you can see in the example.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBbars** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
