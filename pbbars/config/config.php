<?php


// =====================================================================
// Initialize the data array.
// =====================================================================
$data = array();


// =====================================================================
// Specify the graphics settings - Display type.
//
//    0 = The graphic will be sized to match the specified width also
//        if the graphic scale is a little bit distorted. This happens
//        because one pixel is the smallest unit available and can
//        not be divided --> The size of the graph is more important
//        than the precision of the graph.
//
//    1 = The graphic will be sized so that the values and scale match
//        and are not distorted. The size of the graphic can therefore
//        be a little bit smaller than the specified width --> The
//        precision of the graph is more important than the size of the
//        graph.
//
// =====================================================================
$data['settings']['display_type'] = 0;


// =====================================================================
// Specify the graphics settings - Title.
// =====================================================================
$data['settings']['title'] = "Product Sales 2017";


// =====================================================================
// Specify the graphics settings - Scale text.
// =====================================================================
$data['settings']['scale'] = "Percent:";


// =====================================================================
// Specify the graphics settings - Width of the graphic bars.
// =====================================================================
$data['settings']['max_width'] = 400;


// =====================================================================
// Specify the graphics settings - Font size.
// =====================================================================
$data['settings']['font_size'] = 10;


// =====================================================================
// Specify the graphics settings - Padding.
// =====================================================================
$data['settings']['padding'] = 3;
$data['settings']['decimal']['value'] = 0;
$data['settings']['decimal']['percent'] = 1;


// =====================================================================
// Specify the alignment of the data.
// =====================================================================
$data['settings']['alignment']['bar'] = "left";
$data['settings']['alignment']['cell'] = "left";


// =====================================================================
// Specify the position of the data (before, inside, outside, after).
// =====================================================================
$data['settings']['position']['text'] = "before";
$data['settings']['position']['value'] = "inside";
$data['settings']['position']['percent'] = "outside";


// =====================================================================
// Specify which data is displayed.
// =====================================================================
$data['settings']['display']['text'] = 1;
$data['settings']['display']['value'] = 1;
$data['settings']['display']['percent'] = 1;


// =====================================================================
// Specify the graphics data - Bar 0.
// =====================================================================
$data['value'][0] = 30;
$data['text'][0] = "Product A";
$data['color']['text'][0] = "#003973";
$data['color']['bar'][0] = "#6E00C2";
$data['color']['back'][0] = "#E77FFF";
$data['color']['border'][0] = "#E057FF";


// =====================================================================
// Specify the graphics data - Bar 1.
// =====================================================================
$data['value'][1] = 15;
$data['text'][1] = "Product B";
$data['color']['text'][1] = "#003973";
$data['color']['bar'][1] = "#AB0060";
$data['color']['back'][1] = "#FF7FB0";
$data['color']['border'][1] = "#FF5999";


// =====================================================================
// Specify the graphics data - Bar 2.
// =====================================================================
$data['value'][2] = 5;
$data['text'][2] = "Product C";
$data['color']['text'][2] = "#003973";
$data['color']['bar'][2] = "#CC3300";
$data['color']['back'][2] = "#FF9C7E";
$data['color']['border'][2] = "#FF764D";


// =====================================================================
// Specify the graphics data - Bar 3.
// =====================================================================
$data['value'][3] = 45;
$data['text'][3] = "Product D";
$data['color']['text'][3] = "#003973";
$data['color']['bar'][3] = "#DE6604";
$data['color']['back'][3] = "#FFD77E";
$data['color']['border'][3] = "#FFC74F";


// =====================================================================
// Specify the graphics data - Bar 4.
// =====================================================================
$data['value'][4] = 10;
$data['text'][4] = "Product E";
$data['color']['text'][4] = "#003973";
$data['color']['bar'][4] = "#63821A";
$data['color']['back'][4] = "#CAF562";
$data['color']['border'][4] = "#ABE329";


// =====================================================================
// Specify the graphics data - Bar 5.
// =====================================================================
$data['value'][5] = 20;
$data['text'][5] = "Product F";
$data['color']['text'][5] = "#003973";
$data['color']['bar'][5] = "#259473";
$data['color']['back'][5] = "#62F5C8";
$data['color']['border'][5] = "#4BDEB2";


// =====================================================================
// Specify the graphics data - Bar 6.
// =====================================================================
$data['value'][6] = 80;
$data['text'][6] = "Product G";
$data['color']['text'][6] = "#003973";
$data['color']['bar'][6] = "#1D71AD";
$data['color']['back'][6] = "#7FCAFF";
$data['color']['border'][6] = "#4AB4FF";


// =====================================================================
// Specify the graphics data - Bar 7.
// =====================================================================
$data['value'][7] = 35;
$data['text'][7] = "Product H";
$data['color']['text'][7] = "#003973";
$data['color']['bar'][7] = "#2F449E";
$data['color']['back'][7] = "#7F97FF";
$data['color']['border'][7] = "#5978FF";


// =====================================================================
// Specify the graphics data - Bar 8.
// =====================================================================
$data['value'][8] = 70;
$data['text'][8] = "Product I";
$data['color']['text'][8] = "#003973";
$data['color']['bar'][8] = "#5E419C";
$data['color']['back'][8] = "#A77FFF";
$data['color']['border'][8] = "#8C57FF";


// =====================================================================
// Specify the graphics data - Bar 9.
// =====================================================================
$data['value'][9] = 40;
$data['text'][9] = "Product J";
$data['color']['text'][9] = "#003973";
$data['color']['bar'][9] = "#726E7A";
$data['color']['back'][9] = "#BCB5C9";
$data['color']['border'][9] = "#A29CAD";
